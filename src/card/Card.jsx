import React, {Component} from 'react';
import StarRatingComponent from 'react-star-rating-component';
import '../App.css';

class Card extends Component {
    render(){
        return (
            <div>
                <div className="card">
                    <img id="img" src={this.props.photo} alt="Denim Jeans" />
                    <p id="product_name">{this.props.name}</p>
                    <div id="stars">
                        <StarRatingComponent
                            name={"card"}
                            editing={false}
                            renderStarIcon={() => <span>*</span>}
                            starCount={5}
                            value={this.props.stars}
                        />
                        <span id="vote">{this.props.votes}</span>
                    </div>
                    <p id="price">{this.props.price}</p>
                    <p id={"seller"}>فروشنده: {this.props.seller}</p>
                </div>
            </div>
        )
    }
}

export default Card;