import React from 'react';
import './App.css';
import Card from './card/Card';

function App() {

    const products = require('./json/info');
    return (
    <div className="App">
        {
            products.map((product) =>
                <Card
                    name={product.name}
                    price={product.price}
                    seller={product.seller}
                    stars={product.stars}
                    votes={product.votes}
                    photo={product.photo}
                />
            )
        }
    </div>
  );
}

export default App;
